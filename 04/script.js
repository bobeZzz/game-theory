var generateButton,p1s,p2s,solveButton;
var stgNames = ['A','B','C','D','E','F','G','H','I'];
var p1Matrix = [];
var p2Matrix = [];
window.onload = function () {
    generateButton = document.getElementById('generate');
    generateButton.addEventListener('click',generateGame);
    p1s = document.getElementById('p1s');
    p2s = document.getElementById('p2s');
    solveButton = document.getElementById('solve');
    solveButton.addEventListener('click',solveMatrix);
    solveButton.disabled = true;

};


function generateGame() {
    if(p1s.value>9 || p2s.value>9 || p1s.value<1 || p2s.value<1){
        alert('enter startegies in range from 1 to 9');
        return;
    }
    solveButton.disabled = false;
    var table = document.createElement('table');

    for(var i = 0; i<+p1s.value+1; i++){
        var tr = document.createElement('tr');
        var td = document.createElement('td');
        if(i!=0){
            td.appendChild(document.createTextNode(stgNames[i-1]))
            tr.appendChild(td);
        }
        p1Matrix[i] = [];
        p2Matrix[i] = [];

        for(var k = 0; k<+p2s.value+1; k++){

            var td = document.createElement('td');
            if(i==0 ){
                if(k==0){
                    tr.appendChild(td);
                    continue;
                }
                td.appendChild(document.createTextNode(stgNames[k-1]))
                tr.appendChild(td);
                continue;
            }
            else{
            // write to matrix
                if(k!=0 && i!=0){

                    p1Matrix[i][k] = +getRndInteger(-9,10);
                    p2Matrix[i][k] = +getRndInteger(-9,10);
                    td.appendChild(document.createTextNode(p1Matrix[i][k]+"; "+p2Matrix[i][k]));
                    td.id = i+'-'+k;
                    tr.appendChild(td);

                }
            }
        }
        table.appendChild(tr);
    }

    var oldTableParent = document.getElementById('table');
    var oldResult = document.getElementById('result');
    while (oldTableParent.firstChild) {
        oldTableParent.removeChild(oldTableParent.firstChild);
    }
    while (oldResult.firstChild) {
        oldResult.removeChild(oldResult.firstChild);
    }
    table.id = 'matrix';
    table.addEventListener('click',leftClick);
    table.addEventListener('contextmenu',rightClick);
    oldTableParent.appendChild(table)
}

function solveMatrix() {
    solveButton.disabled= true;
    var table = document.getElementById('matrix').cloneNode(true);
    // reaction of second
    for(var i = 1; i<+p1s.value+1;i++){
        var val = Math.max.apply(null, p2Matrix[i].slice(1,p2Matrix[i].length))
        var idx = p2Matrix[i].indexOf(+val);
        console.log(idx);
        while (idx != -1) {
            var tr = table.getElementsByTagName('tr')[i];
            var td = tr.getElementsByTagName('td')[idx];
            console.log('1');
            console.log(td);
            td.classList.toggle('s2r');
            idx = p2Matrix.indexOf(val, idx + 1);
        }
    }

    for(var i = 1; i<+p2s.value+1;i++){
        var val = Math.max.apply(null, p1Matrix[i].slice(1,p1Matrix[i].length))
        var idx = p1Matrix[i].indexOf(+val);
        console.log(idx);
        while (idx != -1) {
            var tr = table.getElementsByTagName('tr')[i];
            var td = tr.getElementsByTagName('td')[idx];
            console.log('1');
            console.log(td);
            td.classList.toggle('s1r');
            if(td.classList.length==2){
                td.classList.toggle('s2r');
                td.classList.toggle('s1r');
                td.classList.toggle('smr');

            }
            idx = p2Matrix.indexOf(val, idx + 1);
        }
    }

    document.getElementById('result').appendChild(table);
}

function leftClick(ev) {
    if(ev.target.id == '')
        return;
    var td = ev.target;

    td.classList.toggle('s1r');

    if(td.classList.length >= 2){
        td.classList.toggle('s1r');
        td.classList.toggle('s2r');

        td.classList.toggle('smr');
    }

}

function rightClick(ev) {
    if(ev.target.id == '')
        return;
    var td = ev.target;

    td.classList.toggle('s2r');

    if(td.classList.length >= 2){
        td.classList.toggle('s1r');
        td.classList.toggle('s2r');

        td.classList.toggle('smr');
    }

    ev.preventDefault();
}


function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

