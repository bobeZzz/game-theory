﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace NashЕquilibrium
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int strategies1Count;
        int strategies2Count;
        int basePayment;
        int topPayment;
        gameItem[,] matrix;
        gameItem[,] startMatrix;
        bool marked, p1Turn = true;
        int rows;
        int cols;
        int rowToDelete = -1, colToDelete = -1;
        history hw;
        history hwp;

        public MainWindow()
        {
            InitializeComponent();
            hw = new history();
            hw.Title = "Auto solution";
            hw.Show();
            hwp = new history();
            hwp.Title = "Player choices";
            hwp.Show();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            basePayment = int.Parse(basePaymentTextBox.Text);
            topPayment = int.Parse(topPaymentTextBox.Text);
            solveButton.IsEnabled = false;

            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0 && App.Current.Windows[intCounter].Title != "MainWindow"; intCounter--)
                App.Current.Windows[intCounter].Close();
        }

        private void strategyCount_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;
                return;
            }
            var x = int.Parse(e.Text);
            if (x == 0)
                e.Handled = true;
        }

        private void paymentInput_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex(@"\d|-");
            e.Handled = !regex.IsMatch(e.Text);
        }

        private void generateStrategies_Click(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(strategies1CountTextBox.Text, out strategies1Count) ||
                !int.TryParse(strategies2CountTextBox.Text, out strategies2Count) ||
                !int.TryParse(basePaymentTextBox.Text, out basePayment) ||
                !int.TryParse(topPaymentTextBox.Text, out topPayment) ||
                !(basePayment < topPayment))
            {
                MessageBox.Show("input error");
                return;
            }

            rows = strategies1Count;
            cols = strategies2Count;

            randomMatrix();
            drawGrid();

            //
            historyMatrixCounter = 0;
            hw.grid.Children.Clear();
            hwp.grid.Children.Clear();
            string gridXaml = XamlWriter.Save(myGrid);
            //
            StringReader stringReader = new StringReader(gridXaml);
            XmlReader xmlReader = XmlReader.Create(stringReader);
            Grid cloneGrid = (Grid)XamlReader.Load(xmlReader);
            cloneGrid.Height = 400;
            cloneGrid.VerticalAlignment = VerticalAlignment.Top;
            cloneGrid.Margin = new Thickness(0, historyMatrixCounter * 400, 0, 0);
            hw.grid.Children.Add(cloneGrid);
            gridXaml = XamlWriter.Save(myGrid);
            //
            stringReader = new StringReader(gridXaml);
            xmlReader = XmlReader.Create(stringReader);
            cloneGrid = (Grid)XamlReader.Load(xmlReader);
            cloneGrid.Height = 400;
            cloneGrid.VerticalAlignment = VerticalAlignment.Top;
            cloneGrid.Margin = new Thickness(0, historyMatrixCounter * 400, 0, 0);

            hwp.grid.Children.Add(cloneGrid);
            //

            marked = false;
            p1Turn = true;

            solveButton.IsEnabled = true;
            stackCounter = 0;
        }

        private void drawGrid(int rowBlock = -1, int colBlock = -1,bool drawFirstMatrix = false)
        {
            
            var tempMargin = myGrid.Margin;

            myGrid.Children.Clear();
            myGrid.ColumnDefinitions.Clear();
            myGrid.RowDefinitions.Clear();

            myGrid = new Grid();
            myGrid.ShowGridLines = true;
            myGrid.Margin = tempMargin;


            for (int i = 0; i <= rows; i++)
            {
                myGrid.RowDefinitions.Add(new RowDefinition());
            }

            for (int i = 0; i <= cols; i++)
            {
                myGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }


            //for (int i = 1; i <= rows; i++)
            //{
            //    //adding row headings
            //    if (i != rowBlock)
            //    {
            //        TextBlock t = new TextBlock();
            //        t.Text = "S" + i;
            //        t.FontSize = 14;
            //        t.FontWeight = FontWeights.Bold;
            //        t.VerticalAlignment = VerticalAlignment.Center;
            //        t.HorizontalAlignment = HorizontalAlignment.Center;
            //        Grid.SetRow(t, i);
            //        Grid.SetColumn(t, 0);
            //        myGrid.Children.Add(t);
            //    }
            //}

            //for (int i = 1; i <= cols; i++)
            //{
            //    //col
            //    if (i != colBlock)
            //    {
            //        TextBlock t = new TextBlock();
            //        t.Text = "S" + i;
            //        t.FontSize = 14;
            //        t.FontWeight = FontWeights.Bold;
            //        t.VerticalAlignment = VerticalAlignment.Center;
            //        t.HorizontalAlignment = HorizontalAlignment.Center;
            //        Grid.SetRow(t, 0);
            //        Grid.SetColumn(t, i);
            //        myGrid.Children.Add(t);
            //    }
            //}


            for (int i = 0; i < rows; i++)
            {
                //rows
                TextBlock t = new TextBlock();
                t.Text = "P1S" + matrix[i, 0].p1s.n;
                t.FontSize = 20;
                t.PreviewMouseDown += rowDeleteHandler;
                t.FontWeight = FontWeights.Bold;
                t.VerticalAlignment = VerticalAlignment.Center;
                t.HorizontalAlignment = HorizontalAlignment.Center;
                Grid.SetRow(t, i + 1);
                Grid.SetColumn(t, 0);
                myGrid.Children.Add(t);

                for (int k = 0; k < cols; k++)
                {

                    if (i == 0)
                    {
                        //cols
                        t = new TextBlock();
                        t.Text = "P2S" + matrix[0, k].p2s.n;
                        t.FontSize = 20;
                        t.PreviewMouseDown += columnToDeteleHandler;
                        t.FontWeight = FontWeights.Bold;
                        t.VerticalAlignment = VerticalAlignment.Center;
                        t.HorizontalAlignment = HorizontalAlignment.Center;
                        Grid.SetRow(t, 0);
                        Grid.SetColumn(t, k + 1);
                        myGrid.Children.Add(t);
                    }

                    Grid.SetRow(matrix[i, k].tb, i + 1);
                    Grid.SetColumn(matrix[i, k].tb, k + 1);
                    myGrid.Children.Add(matrix[i, k].tb);
                }
            }

            mainGrid.Children.Add(myGrid);
        }
        int ctdh = 1;
        private void columnToDeteleHandler(object sender, MouseButtonEventArgs e)
        {

            var t = (TextBlock)sender;
            var a = (int)Char.GetNumericValue(t.Text[3]);
            int ind = a - ctdh;
            //delete col

            if (matrix.GetLength(1) == 1)
                return;
            historyMatrixCounter++;

            matrix = TrimMatrix(matrix, -1, ind);
            drawGrid(-1, ind + 1);
            colToDelete = -1;


            string gridXaml = XamlWriter.Save(myGrid);
            //
            StringReader stringReader = new StringReader(gridXaml);
            XmlReader xmlReader = XmlReader.Create(stringReader);
            Grid cloneGrid = (Grid)XamlReader.Load(xmlReader);
            cloneGrid.Height = 400;
            cloneGrid.VerticalAlignment = VerticalAlignment.Top;
            cloneGrid.Margin = new Thickness(0, historyMatrixCounter * 400 + 50, 0, 0);
            hwp.grid.Children.Add(cloneGrid);
            ctdh++;
        }
        int rdh = 1;
        private void rowDeleteHandler(object sender, MouseButtonEventArgs e)
        {
            var t = (TextBlock)sender;
            var a = (int)Char.GetNumericValue(t.Text[3]);
            int ind = a - rdh;
            //delete row
            if (matrix.GetLength(0) == 1)
                return;
            historyMatrixCounter++;

            matrix = TrimMatrix(matrix, ind, -1);
            drawGrid(ind + 1);
            rowToDelete = -1;

            string gridXaml = XamlWriter.Save(myGrid);
            //
            StringReader stringReader = new StringReader(gridXaml);
            XmlReader xmlReader = XmlReader.Create(stringReader);
            Grid cloneGrid = (Grid)XamlReader.Load(xmlReader);
            cloneGrid.Height = 400;
            cloneGrid.VerticalAlignment = VerticalAlignment.Top;
            cloneGrid.Margin = new Thickness(0, historyMatrixCounter * 400 + 50, 0, 0);
            hwp.grid.Children.Add(cloneGrid);
            rdh++;
        }

        private void randomMatrix()
        {
            Random rnd = new Random(DateTime.Now.Millisecond);
            matrix = new gameItem[strategies1Count, strategies2Count];
            startMatrix = new gameItem[strategies1Count, strategies2Count];
            for (int i = 0; i < rows; i++)
            {
                for (int k = 0; k < cols; k++)
                {
                    var v1 = rnd.Next(basePayment, topPayment + 1);
                    var v2 = rnd.Next(basePayment, topPayment + 1);
                    matrix[i, k] = new gameItem(v1, v2);
                    matrix[i, k].p1s.n = i + 1;
                    matrix[i, k].p2s.n = k + 1;

                    startMatrix[i, k] = new gameItem(v1, v2);
                    startMatrix[i, k].p1s.n = i + 1;
                    startMatrix[i, k].p2s.n = k + 1;
                }
            }

        }

        int stackCounter = 0;
        int historyMatrixCounter = 0;
        private void solveButton_Click(object sender, RoutedEventArgs e)
        {
            historyMatrixCounter = 0;
            rows = strategies1Count;
            cols = strategies2Count;
            stackCounter = 0;
            p1Turn = true;
            marked = false;
            rowToDelete = -1;
            colToDelete = -1;
            matrix = startMatrix;
            drawGrid(drawFirstMatrix: true);
            
            var res = oldsolveFunction();
            while (res != int.MaxValue)
            {
                res = oldsolveFunction();
            }
        }

        public int oldsolveFunction()
        {
            if (rows == 1 && cols == 1)
                return -1;
            bool stronglyDominated = false;
            if (!marked)//!marked
            {
                stackCounter++;
                if (stackCounter == 3)
                {
                    MessageBox.Show("Найбільш раціональна стратегія відсутня");
                    solveButton.IsEnabled = false;
                    return int.MaxValue;
                }
                if (p1Turn)//p1Turn
                {
                    //dlya p1-
                    for (int i = 0; i < rows && !stronglyDominated; i++)
                    {
                        for (int q = 0; q < rows && !stronglyDominated; q++)
                        {
                            int counter = 0;
                            for (int z = 0; z < cols && !stronglyDominated; z++)
                            {
                                if (q == i)
                                    continue;
                                else
                                {
                                    if (startMatrix[i, z].p1s.value < startMatrix[q, z].p1s.value)
                                        counter++;
                                }
                            }

                            if (counter == cols)
                            {
                                var row = myGrid.Children.Cast<UIElement>().First(ex => Grid.GetRow(ex) == i + 1);
                                var whatever = row.GetType().GetProperty("Background").GetValue(row);
                                row.GetType().GetProperty("Background").SetValue(row, Brushes.Red);
                                rowToDelete = i;
                                marked = true;
                                stronglyDominated = true;
                            }
                        }
                    }
                    //dlya p1-
                    p1Turn = false;
                }
                else
                {
                    for (int i = 0; i < cols && !stronglyDominated; i++)
                    {
                        for (int q = 0; q < cols && !stronglyDominated; q++)
                        {
                            int counter = 0;
                            for (int z = 0; z < rows && !stronglyDominated; z++)
                            {
                                if (q == i)
                                    continue;
                                else
                                {
                                    if (startMatrix[z, i].p2s.value < startMatrix[z, q].p2s.value)
                                        counter++;
                                }
                            }

                            if (counter == rows)
                            {
                                var row = myGrid.Children.Cast<UIElement>().First(ex => Grid.GetColumn(ex) == i + 1);
                                var whatever = row.GetType().GetProperty("Background").GetValue(row);
                                row.GetType().GetProperty("Background").SetValue(row, Brushes.Red);
                                colToDelete = i;
                                marked = true;
                                stronglyDominated = true;
                            }
                        }
                    }
                    p1Turn = true;

                }

            }
            else
            {
                if (rowToDelete != -1 || colToDelete != -1)
                {
                    if (!p1Turn)
                    {
                        //delete row
                        startMatrix = TrimMatrix(startMatrix, rowToDelete, -1);
                        drawGrid(rowToDelete + 1);
                        rowToDelete = -1;
                    }
                    else
                    {
                        //delete col
                        startMatrix = TrimMatrix(startMatrix, -1, colToDelete);
                        drawGrid(-1, colToDelete + 1);
                        colToDelete = -1;
                    }
                    stackCounter = 0;
                    marked = false;

                    historyMatrixCounter++;
                    //clone current matrix add new grid;
                    string gridXaml = XamlWriter.Save(myGrid);
                    //
                    StringReader stringReader = new StringReader(gridXaml);
                    XmlReader xmlReader = XmlReader.Create(stringReader);
                    Grid cloneGrid = (Grid)XamlReader.Load(xmlReader);
                    cloneGrid.Height = 400;
                    cloneGrid.VerticalAlignment = VerticalAlignment.Top;
                    cloneGrid.Margin = new Thickness(0, historyMatrixCounter * 400 + 50, 0, 0);
                    hw.grid.Children.Add(cloneGrid);

                }
            }

            if (startMatrix.GetLength(0) == 1 && startMatrix.GetLength(1) == 1)
            {
                stackCounter = 3;
                solveButton.IsEnabled = false;
                MessageBox.Show("Найбільш раціоанальну стратегію знайдено");

                return int.MaxValue;
            }
            return -1;
        }



        public gameItem[,] TrimMatrix(gameItem[,] originalArray, int rowToRemove = -1, int columnToRemove = -1)
        {
            int nCol = -1, nRow = -1;
            if (rowToRemove != -1)
            {
                nRow = originalArray.GetLength(0) - 1;
                rows--;
            }
            else
                nRow = originalArray.GetLength(0);

            if (columnToRemove != -1)
            {
                nCol = originalArray.GetLength(1) - 1;
                cols--;
            }
            else
                nCol = originalArray.GetLength(1);

            gameItem[,] result = new gameItem[nRow, nCol];

            for (int i = 0, j = 0; i < originalArray.GetLength(0); i++)
            {
                if (i == rowToRemove)
                    continue;

                for (int k = 0, u = 0; k < originalArray.GetLength(1); k++)
                {
                    if (k == columnToRemove)
                        continue;

                    result[j, u] = originalArray[i, k];
                    u++;
                }
                j++;
            }

            return result;
        }

        public class gameItem
        {
            public strat p1s { get; set; }
            public strat p2s { get; set; }
            public TextBlock tb { get; set; }

            public gameItem(int p1s, int p2s)
            {
                this.p1s = new strat();
                this.p2s = new strat();

                this.p1s.value = p1s;
                this.p2s.value = p2s;

                tb = new TextBlock();
                tb.Text = this.ToString();
                tb.FontSize = 20;
                tb.VerticalAlignment = VerticalAlignment.Center;
                tb.HorizontalAlignment = HorizontalAlignment.Center;
            }

            public override string ToString()
            {
                return p1s.value + "; " + p2s.value;
            }
        }

        public class strat
        {
            public int value { get; set; }
            public int n { get; set; }
        }


    }
}
